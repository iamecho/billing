-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 06, 2017 at 12:44 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `billing`
--

-- --------------------------------------------------------

--
-- Table structure for table `billing_account`
--

CREATE TABLE `billing_account` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `address` varchar(300) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(14) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `billing_account`
--

INSERT INTO `billing_account` (`id`, `first_name`, `last_name`, `address`, `email`, `phone`) VALUES
(1, 'string', 'string', '{}', 'string', 'string'),
(2, 'John', 'Gordon', '{}', 'Jagordon09@gmail.com', '876-311-1123');

-- --------------------------------------------------------

--
-- Table structure for table `billing_currency`
--

CREATE TABLE `billing_currency` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `symbol` varchar(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `billing_currency`
--

INSERT INTO `billing_currency` (`id`, `name`, `symbol`) VALUES
(1, 'JMD', '$');

-- --------------------------------------------------------

--
-- Table structure for table `billing_cycle_type`
--

CREATE TABLE `billing_cycle_type` (
  `id` int(11) NOT NULL,
  `name` varchar(14) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `billing_cycle_type`
--

INSERT INTO `billing_cycle_type` (`id`, `name`) VALUES
(1, 'One Time');

-- --------------------------------------------------------

--
-- Table structure for table `billing_group`
--

CREATE TABLE `billing_group` (
  `id` int(11) NOT NULL,
  `billing_currency_id` int(11) NOT NULL,
  `billing_tax_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` int(11) NOT NULL,
  `price` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `billing_status`
--

CREATE TABLE `billing_status` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `billing_tax`
--

CREATE TABLE `billing_tax` (
  `id` int(11) NOT NULL,
  `tax` double NOT NULL,
  `jurisdiction` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `billing_transaction`
--

CREATE TABLE `billing_transaction` (
  `id` int(11) NOT NULL,
  `billing_group_id` int(11) NOT NULL,
  `billing_type_id` int(11) NOT NULL,
  `billing_account_id` int(11) NOT NULL,
  `billing_cycle_id` int(11) NOT NULL,
  `billing_status_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `description` varchar(1000) NOT NULL,
  `billing_type_detail` varchar(1000) NOT NULL,
  `billing_type_response` varchar(1000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `billing_type`
--

CREATE TABLE `billing_type` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `billing_account`
--
ALTER TABLE `billing_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_currency`
--
ALTER TABLE `billing_currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_cycle_type`
--
ALTER TABLE `billing_cycle_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_group`
--
ALTER TABLE `billing_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_status`
--
ALTER TABLE `billing_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_tax`
--
ALTER TABLE `billing_tax`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_transaction`
--
ALTER TABLE `billing_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_type`
--
ALTER TABLE `billing_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `billing_account`
--
ALTER TABLE `billing_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `billing_currency`
--
ALTER TABLE `billing_currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `billing_cycle_type`
--
ALTER TABLE `billing_cycle_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `billing_group`
--
ALTER TABLE `billing_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `billing_status`
--
ALTER TABLE `billing_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `billing_tax`
--
ALTER TABLE `billing_tax`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `billing_transaction`
--
ALTER TABLE `billing_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `billing_type`
--
ALTER TABLE `billing_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
